const express= require("express");

// this lets us use mongoose module

const mongoose= require("mongoose");

const app =express ();

const port = 3000;


// MongoDB connection
/*	

	-connect to the data by passing in your connection string ("from mongoDB")
	- ".connect()" lets ys connect and access the database that is sent to it via string
	-"b190-to-do" is the database that we have created in our mongoDB

	*/
mongoose.connect("mongodb+srv://shielamaemgonzales:gonzales@wdc028-course-booking.b55st6r.mongodb.net/b190-to-do?retryWrites=true&w=majority",
	
	/*


		-this will not prevent mongoose from being used in the application and sending unmecessary warnings when
		we send requests
	*/

	{
		useNewUrlParser: true,
		useUnifiedTopology: true
	}


);

/* 	-sets notifications for connection sucess or failure
	-allows us to handles errors when initial connection is established
	- works with on and once mongoose methods
*/

let db = mongoose.connection;
/*

	-if a connection error occured, we will be seeing it in the console.
	-console.error.bind allows us to print errors in the browser as well as in the
	terminal

*/
db.on("error",console.error.bind(console,"connection error"));

/*if the connection is successful, it is confirmed in the console.*/

db.once("open",()=>console.log("we're connected to the database"));

/*

	-Schema() is a method inside the mongoose module that let us cerate schema
	for our database it receives an object with properties and data types that 
	each property should have/

*/
// SCHEMA SECTION
const taskSchema=new mongoose.Schema({

/* the "name" property should receive a string data type 
(it should be written in Sentence case)*/
	name: String,

/*the dfault property allows the server to automatically assign a value to the property
once the user fails to provide one*/
	
	status:{
		type: String,
		default: "pending"
	}
});


// SECTION- MODELS


/* 

		-models in mongoose use schemas and are used to complete the object instantiation
		that correspond to that schema

		-models use Schemas and they act as the middleman from the server to the
		database

		-first parameter is the collection in where to store the data
		
		- second parameter is used to specify the Schema/ blueprint of the
		documents that will be stored in the MongoDB collection

		-Models must be witten in singular form, sentence case

		-the "Task" variable now will be used for us to run commands for interacting
		with our database


*/
const Task=mongoose.model("Task",taskSchema);



/*allows handling of json data structure*/

app.use(express.json());

/*receives all kinds of data*/

app.use(express.urlencoded({extended:true}));

/*Create new task*/


/*BUSINESS LOGIC

	-add a functionality that will check if there are duplicate tasks
	-if the task is already existing, we return an error
	-if the task is not existing, we add it in the database

	- the task will be sent from the request body
	- create a new Task object with a "name" field/property
	-the "status" property does not need to be provided because our schema default is to
	"pending" upon creation of an object

*/

app.post("/tasks",(req,res) => {

	Task.findOne({name: req.body.name}, (err,result)=>{
		if (result!== null && result.name === req.body.name){
			return res.send("Duplicate task found");
		} else {
			let newTask = new Task({
				name: req.body.name
			});
		newTask.save((saveErr,savedTask) =>{
				if (saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New task created");
				};
			});
		};
	});
});


app.get("/tasks",(req,res)=>{
	Task.find({}, (err,result) => {
		if(result === null){
			return res.send("Error")
		}else{
			res.send(result)
		}
	})
})

app.listen(port,() => console.log(`Server running at port: ${port}`));





// ACTIVITY


/*
Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a git repository named S35.
6. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
7. Add the link in Boodle.*/


/*Create a User schema.*/
const UserSchema=new mongoose.Schema({
	name: String,
	status:{
		type: String,
		default: "pending"
	}
});

// 2. Create a User model.
const User=mongoose.model("User",UserSchema);


app.use(express.json());


app.use(express.urlencoded({extended:true}));

//  Create a POST route that will access the "/signup" route that will create a user.

app.post("/signup",(req,res) => {

	Task.findOne({name: req.body.name}, (err,result)=>{
		if (result!== null && result.name === req.body.name){
			return res.send("Duplicate user found");
		} else {
			let newUser = new User({
				name: req.body.name
			});
		newUser.save((saveErr,savedUser) =>{
				if (saveErr){
					return console.error(saveErr);
				} else {
					return res.status(201).send("New user created.");
				};
			});
		};
	});
});